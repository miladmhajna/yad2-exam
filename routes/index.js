const express = require('express')
const router = express.Router()

const bodyParser = require('body-parser')
let jsonParser = bodyParser.json()

const chat = require('../models/chat')

router.get('/chat', (req, res) => {

    const { deleted } = req.query

    let filter = {}
    
    if(deleted)
        filter.deleted = deleted

    chat.find(filter, (err, result) => {
        return res.json(result)
    })
})

router.post('/chat', jsonParser, (req, res) => {

    let message = new chat({
        sender: req.body.sender,
        msg: req.body.msg
    });

    // save message to db
    message.save((err, result) => {

        if (err) return res.send(err)

        return res.json({status: true, data: result})
    })
})

router.delete('/chat/:id', (req, res) => {
    const filter = { _id: req.params.id };
    const update = { deleted: 1 };

    chat.findOneAndUpdate(filter, update, {returnOriginal: false})
    .then(function(result){
        return res.json({status: true, data: result})
    }).catch(function(error){
        console.log(error); // Failure
        return res.send(err)
    });
})

module.exports = router


