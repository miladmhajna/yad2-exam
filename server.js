const app = require('express')()
const http = require('http').Server(app)
const io = require('socket.io')(http, {cors: {origin: '*'}})
const port = 3000

// hondle db
require('./config/db')

//handle socket
require('./config/skt')(io)

// handle routes
const router = require('./routes/index.js')
app.use(router)

http.listen(port, () => {
    console.log(`Listening on port ${port}`)
})
