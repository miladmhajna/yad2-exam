import io from 'socket.io-client';

export default class Socket {
    constructor(url) {
        this.url = url
        this.io = io(url)
    }

    fire(event, data) {
        this.io.emit(event, data)
    }

    listen(event, callback) {
        this.io.on(event, data => {
            callback && typeof callback === 'function' && callback(data)
        })
    }
}