const mongoose = require('mongoose');
const { Schema } = mongoose;

// create schema for chat
chatSchema = Schema({
    sender: {type: String, required: true},
    msg: {type: String, required: true},
    date: {type: Date, default: Date.now},
    deleted: {type: Boolean, default: 0},
})

// create model for chat
chat = mongoose.model('chat', chatSchema)

module.exports = chat