const mongoose = require('mongoose')

const host = '127.0.0.1'
const port = '27017'
const dbname = 'yad2-chat'
const dbhost = `mongodb://${host}:${port}/${dbname}`

mongoose.connect(dbhost)

module.exports = mongoose