const chat = require('../models/chat')

module.exports = function(io) {
    let users = []

    // handle on connection
    io.on('connection', socket => {
        connected(socket)
        newUser(socket)
        sendMessage(socket)
        deleteMessage(socket)
        disconnect(socket)
    })

    // handle when user connected
    connected = async socket => {
        const messages = await chat.find()
        
        socket.emit('connected', { 
            users: users.map(u => u.username),
            messages
        })
    }

    // handle new user
    newUser = socket => {
        socket.on('new-user', username => {
            console.log(`${username} has joined.`)

            socket.username = username
            // add user to list
            users.push(socket)

            //fire event to front
            io.emit('user-online', username)
        })
    }

    // handle when send message
    sendMessage = socket => {
        socket.on('send-message', msg => {
            // create new message chat
            let message = new chat({
                sender: socket.username,
                msg: msg
            });

            // save message to db
            message.save((err, result) => {
                if (err) throw err;

                //fire event to front
                io.emit('message-sent', message)
            })

        })
    }
    
    deleteMessage = socket => {
        socket.on('delete-message', id => {
            const filter = { _id: id };
            const update = { deleted: 1 };

            chat.findOneAndUpdate(filter, update, {returnOriginal: false})
            .then(function(result){
                io.emit('message-deleted', result)
            }).catch(function(error){
                console.log(error); // Failure
                throw err
            });
        })
    }

    // handle disconnect user
    disconnect = socket => {
        socket.on('disconnect', () => {
            console.log(`${socket.username} has left.`)

            //fire event to front
            io.emit('user-left', socket.username)

            // remove user from list
            const userIndex = users.indexOf(socket)
            users.splice(userIndex, 1)
        })
    }
}